#!/usr/bin/python3

import csv
import sys
import jinja2

if not sys.argv[-1].endswith(".csv"):
    raise Exception("Please pass a csv file as argument.")

with open(sys.argv[-1], newline="") as fh:
    b = csv.DictReader(fh)
    k = list(b)
    templateEnv = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath="./"))
    template = templateEnv.get_template("phonebook.xml.j2")
    print(template.render(contacts=k))
